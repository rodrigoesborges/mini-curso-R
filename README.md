# Mini Curso de introdução ao R - UFES 2019 (PolSoc)

## Introdução a análise de dados no R

### Estrutura do curso

#### A - O QUE É O R? CIÊNCIA DE DADOS E BOAS FERRAMENTAS

1. Por que R? Uma primeira aproximação a partir de exemplos
    1.1 Exemplos 'abstratos' do que o R pode fazer:
          - De todos(muitos) os formatos de entrada para todos(muitos) formatos de saída
              -"Todos" os formatos de entrada: txt, xls,csv, zip, sql, doc, pdf, rdf, odbc, dados de outros softwares(ex SPSS, Stata, SAS) etc
              - "Todos" os formatos de saída: pdf, html, webapp, ligar-pedir uma pizza etc
    1.2 Exemplos concretos
        - Livro do ibpad <- feito com R ; (integração com Latex, possibilidade de fazer artigos completos)
        - Painel de Conjuntura
        - Painel da América Latina
        - Análise de Redes - whatsapp (semântica/rede social)
        - Microdados da POF e outros - Por quê não sair do critério _oficial_ da renda?
    
    1.2 - Como apresentar resultados de pesquisas ou trabalhos em andamento com dados? Possibilidades de avanço coletivo nessas frentes - explorar e descobrir/confirmar
  
    
    
2. "Ciência de Dados" e o que pode ensinar

    2.1 A era da informática e da *informação*

    2.2 Fluxo de trabalho empírico contemporâneo

        - Síntese 'estática'

        - Síntese dinâmica?

    2.3 Reprodutibilidade - replicabilidade
        
    2.4 Sobre as habilidades necessárias a cultivar (Diagrama de Venn)

        2.4.1 Psicológico
        
            - Hacking <- Gambiarra "bem feita(!)"
            
            - Des-intimidar
            
        2.4.2 Como buscar ajuda:
        
            - Função help() ou `?`
            
            - StackOverflow em Português

            - Fóruns/Grupos/Amigos
            
        2.4.3 Ao lado e além do R
            
            - A linha de comando
                - árvore
                - setas
                - tab
                - exemplos de comandos básicos (pwd, clear, ls, cd, mkdir, touch, cp, rm, mv, date, echo)
            
            - Git / Github/gitlab
                - o principal são arquivos de texto
                - os dados 'binários'


#### B - Conceitos Básicos e o ambiente R

3. Por que R? R e RStudio: Para que contribuem em "ciência de dados"? Como são esses "monstros"?

4. O R como calculadora: operadores básicos e sintaxe
    4.1 soma, multiplicação, divisão, potência
    4.2 assignação

5. Variáveis: tipos e características

    5.1 Escalares
        - operando com escalares

    5.2 Vetores

        - numéricos

        - de caracteres

        - lógicos
        
        - Operando com vetores: especificidades, coerção e reciclagem
        
    5.3 Fatores

    5.4 Listas e Data Frames
    
    5.5 Matrizes (e "Arranjos")
  
    
6. Lendo e gravando dados

    6.1. Formatos comuns:

    a) .csv; 

    b) .txt; 

    c) .xlsx;

    d) bases de dados.
    
    6.2 Formatos do R:

    a) .RDA e 

    b) .RDS 


7. Manipulando dados com o R básico

    7.1. funções subset, with, Filter, `[` e `[[`
    

========= futuramente ? ===============    
8. Manipulando dados com o dplyr

    8.1. Uma gramática para manipulação de dados

    8.2. Os verbos do dplyr:

    * group_by, filter, select, summarise, mutate, arrange
    
9. Introdução a visualização de dados: ggplot2

========= futuramente? ===============

#### C - Exercícios práticos
10. Ex aplicado 1 - CadÚnico

    10.1. Recuperando os dados

    10.2. Usando os dados

11. Ex aplicado 2 - Assistência estudantil IFES

    11.1  Recuperando os dados [link](http://www.portaldatransparencia.gov.br/programas-e-acoes/acao/2994-assistencia-aos-estudantes-das-instituicoes-federais-de-educacao-profissional-e-tecnologica?ano=2017)
    
    11.2  Usando os dados






