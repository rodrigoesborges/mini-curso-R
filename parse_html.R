library(stringr)

cod <- readLines('index.html')

cod <- gsub(pattern = "<[/]?pre><[/]?code>", replacement = "", x = cod)

cod[str_sub(cod, 1,
            3) == "## "] <- str_sub(cod[str_sub(cod, 1,
                                               3) == "## "],
                                   4,
                                   str_length(cod[str_sub(cod, 1,
                                                          3) == "## "]))

cod <- gsub(pattern = "&lt;", replacement = "<", x = cod)
cod <- gsub(pattern = "&gt;", replacement = ">", x = cod)
cod <- gsub(pattern = "&quot;", replacement = '"', x = cod)
cod <- gsub(pattern = "&amp;", replacement = "&", x = cod)
cod <- gsub(pattern = "&#39;", replacement = "'", x = cod)


writeLines(cod, con = 'index.html')  
